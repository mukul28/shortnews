import 'react-native-gesture-handler';
import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {Navigation} from './src/navigation';
// import { StoreContext } from "storeon/react";
import {store, persistor} from './src/redux/store';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
const App = () => {
  return (
    <>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StatusBar barStyle="dark-content" backgroundColor="transparent" />
          <SafeAreaView style={{flex: 1}}>
            <Navigation />
          </SafeAreaView>
        </PersistGate>
      </Provider>
    </>
  );
};

export default App;
