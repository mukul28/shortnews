import {
  forgetPassword,
  login,
  registration,
  verification,
  registration_error,
  login_error,
  logout,
  updateUser,
} from './auth';
import { setCategory, setSelectedCategory } from './category';
import { appendFeeds, setCurrentFeed, setFeeds } from './feed';
export {
  forgetPassword,
  login,
  registration_error,
  registration,
  verification,
  setCategory,
  setSelectedCategory,
  appendFeeds,
  setCurrentFeed,
  setFeeds,
  login_error,
  logout,
  updateUser,
};
