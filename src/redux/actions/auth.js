import {
  LOGIN,
  REGISTRATION,
  FORGATE_PASSWORD,
  VERIFICATION,
  REGISTRATION_ERROR,
  LOGIN_ERROR,
  LOGOUT,
  UPDATE_USER,
} from '../actionTypes';

export function login(payload) {
  return {
    type: LOGIN,
    payload,
  };
}
export function logout(payload) {
  return {
    type: LOGOUT,
    payload,
  };
}
export function registration(payload) {
  return {
    type: REGISTRATION,
    payload,
  };
}
export function registration_error(payload) {
  return {
    type: REGISTRATION_ERROR,
    payload,
  };
}
export function login_error(payload) {
  return {
    type: LOGIN_ERROR,
    payload,
  };
}
export function forgetPassword(payload) {
  return {
    type: FORGATE_PASSWORD,
    payload,
  };
}
export function verification(payload) {
  return {
    type: VERIFICATION,
    payload,
  };
}
export function updateUser(payload) {
  return {
    type: UPDATE_USER,
    payload,
  };
}
