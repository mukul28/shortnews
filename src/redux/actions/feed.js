import {SET_FEEDS, APPEND_FEED, SET_CURRENT_FEED} from '../actionTypes';

export function setFeeds(payload) {
  return {
    type: SET_FEEDS,
    payload,
  };
}
export function appendFeeds(payload) {
  return {
    type: APPEND_FEED,
    payload,
  };
}
export function setCurrentFeed(payload) {
  return {
    type: SET_CURRENT_FEED,
    payload,
  };
}
