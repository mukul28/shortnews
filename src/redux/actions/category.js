import {SET_CATEGORY, SET_SELECTED_CATEGORY} from '../actionTypes';

export function setCategory(payload) {
  return {
    type: SET_CATEGORY,
    payload,
  };
}
export function setSelectedCategory(payload) {
  return {
    type: SET_SELECTED_CATEGORY,
    payload,
  };
}
