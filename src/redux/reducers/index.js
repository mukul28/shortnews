import {combineReducers} from 'redux';
import {authReducer, userReducer} from './auth';
import {categoryReducers} from './category';
import {feedReducers} from './feed';

export default combineReducers({
  authReducer,
  categoryReducers,
  feedReducers,
  userReducer,
});
