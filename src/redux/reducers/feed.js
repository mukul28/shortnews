import {SET_FEEDS, APPEND_FEED, SET_CURRENT_FEED} from '../actionTypes';
const initialState = {
  feeds: [],
  feedCount: 0,
  currentFeeds: {},
};
const feedReducers = (state = initialState, action) => {
  switch (action.type) {
    case SET_FEEDS:
      return Object.assign({}, state, {
        feeds: action.payload.results,
        feedCount: action.payload.count,
      });
    case APPEND_FEED:
      return Object.assign({}, state, {
        feeds: state.feeds.concat(action.payload.results),
        feedCount: action.payload.count,
      });
    case SET_CURRENT_FEED:
      return Object.assign({}, state, {
        currentFeeds: action.payload,
      });
    default:
      return state;
  }
};
export {feedReducers};
