import {
  LOGIN,
  REGISTRATION,
  VERIFICATION,
  FORGATE_PASSWORD,
  REGISTRATION_ERROR,
  LOGIN_ERROR,
  LOGOUT,
  UPDATE_USER,
} from '../actionTypes';

const initialState = {
  user: [],
  registrationError: {},
  loginError: [],
};
const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTRATION_ERROR:
      return Object.assign({}, state, {
        registrationError: action.payload,
      });
    case LOGIN_ERROR:
      return Object.assign({}, state, {
        loginError: action.payload,
      });

    default:
      return state;
  }
};
const userReducer = (state = {user: [], accessToken: null}, action) => {
  switch (action.type) {
    case LOGIN:
      return Object.assign({}, state, {
        user: action.payload.user,
        accessToken: action.payload.token.auth_token,
      });
    case REGISTRATION_ERROR:
      return Object.assign({}, state, {
        registrationError: action.payload,
      });
    case UPDATE_USER:
      return Object.assign({}, state, {
        user: action.payload,
      });
    default:
      return state;
  }
};
export {authReducer, userReducer};
