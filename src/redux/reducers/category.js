import {SET_CATEGORY, SET_SELECTED_CATEGORY} from '../actionTypes';
const initialState = {
  category: [],
  selectedCategory: {},
};
const categoryReducers = (state = initialState, action) => {
  switch (action.type) {
    case SET_CATEGORY:
      return Object.assign({}, state, {
        category: action.payload,
      });
    case SET_SELECTED_CATEGORY:
      return Object.assign({}, state, {
        selectedCategory: action.payload,
      });
    default:
      return state;
  }
};
export {categoryReducers};
