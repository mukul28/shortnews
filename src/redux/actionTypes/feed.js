export const SET_FEEDS = 'SET_FEEDS';
export const APPEND_FEED = 'APPEND_FEED';
export const SET_CURRENT_FEED = 'SET_CURRENT_FEED';
