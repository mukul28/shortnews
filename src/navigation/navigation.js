import React, {useReducer, createContext} from 'react';
import {View, ActivityIndicator} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import {
  registration,
  registration_error,
  login,
  login_error,
} from '../redux/actions';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';

import {
  Login,
  Registration,
  Feed,
  Category,
  Otp,
  Profile,
  ForgetPassword,
  EditProfile,
} from '../screens';
import api from '../api';
import SplashScreen from 'react-native-splash-screen';
export const AuthContext = createContext();

const RootStack = createStackNavigator();
const RootStackScreen = ({userToken}) => (
  <RootStack.Navigator headerMode="none">
    {userToken ? (
      <RootStack.Screen
        name="App"
        component={AppStackScreen}
        options={{
          animationEnabled: false,
        }}
      />
    ) : (
      <RootStack.Screen
        name="Auth"
        component={AuthStackScreen}
        options={{
          animationEnabled: false,
        }}
      />
    )}
  </RootStack.Navigator>
);

const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator screenOptions={{headerShown: false}}>
    <AuthStack.Screen name="Login" component={Login} />
    <AuthStack.Screen
      name="Registration"
      component={Registration}
      options={{title: 'Registration'}}
    />
    <AppStack.Screen name="ForgetPassword" component={ForgetPassword} />
    <AuthStack.Screen name="Otp" component={Otp} options={{title: 'Otp'}} />
  </AuthStack.Navigator>
);

const AppStack = createStackNavigator();
const AppStackScreen = () => (
  <AppStack.Navigator
    initialRouteName={'Category'}
    screenOptions={{
      headerShown: false,
      gestureEnabled: true,
      gestureDirection: 'horizontal',
      cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    }}>
    <AppStack.Screen name="Category" component={Category} />
    <AppStack.Screen name="Feed" component={Feed} />
    <AppStack.Screen name="Profile" component={Profile} />
    <AppStack.Screen name="EditProfile" component={EditProfile} />
  </AppStack.Navigator>
);

export function Navigation() {
  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    },
  );

  React.useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await AsyncStorage.getItem('userToken');
        dispatch({type: 'RESTORE_TOKEN', token: userToken});
        SplashScreen.hide();
      } catch (e) {}
    };

    bootstrapAsync();
  }, []);
  const Dispatch = useDispatch();
  const authContext = React.useMemo(
    () => ({
      signIn: async response => {
        try {
          Dispatch(login(response));
          await AsyncStorage.setItem('userToken', response.token.auth_token);
          dispatch({type: 'SIGN_IN', token: response.token.auth_token});
        } catch (e) {
          console.log(e);
        }
      },
      signOut: async () => {
        try {
          userToken = await AsyncStorage.getItem('userToken');
          console.log('Navigation -> userToken', userToken);
          api.auth.logout(userToken).then(async res => {
            console.log('Navigation -> res', res);
            if (
              res.status === 204 ||
              res.status === 401 ||
              res.status === 403
            ) {
              Dispatch(login({user: {}, token: {auth_token: null}}));
              await AsyncStorage.clear();
              dispatch({type: 'SIGN_OUT'});
            }
            if (res.status === 400) {
              res.json().then(response => {
                console.log('err', response);
              });
            }
          });
        } catch (e) {
          console.log('Navigation -> e', e);
          console.log(e);
        }
      },
      signUp: data => {
        try {
          return true;
          //dispatch({type: 'SIGN_IN', token: 'dummy-auth-token'});
        } catch (e) {
          console.log(e);
        }
      },
    }),
    [],
  );

  if (state.isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator color="#D7962E" size="large" />
      </View>
    );
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStackScreen userToken={state.userToken} />
      </NavigationContainer>
    </AuthContext.Provider>
  );
}
