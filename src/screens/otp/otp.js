import React, {useContext, useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  Platform,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import OTPTextInput from 'react-native-otp-textinput';
import {AuthContext} from '../../navigation/index';
import {ImageUrl} from '../../utils';
import {Colors, Fonts} from '../../theme';
import api from '../../api';
import {HelperText} from 'react-native-paper';
export function Otp({route, navigation}) {
  const {signIn} = useContext(AuthContext);
  const [otp, setOtp] = useState('');
  const [otpError, setOtpError] = useState('');
  const {height} = Dimensions.get('window');
  const [scrollHeight, setScrollHeight] = useState(height);
  const {email} = route.params;
  let otpInput = useRef(null);
  const [isLoading, setIsLoading] = useState(false);
  const onVerifyOTP = () => {
    setIsLoading(true);
    api.auth.otpVarification({otp}, email).then(res => {
      if (res.status === 200) {
        res.json().then(response => {
          if (response.data) {
            let data = {
              token: {
                auth_token: response.data.token,
              },
              user: response.data.user_detail,
            };
            signIn(data);
          }
        });
      }
      if (res.status === 400) {
        res.json().then(error => {
          if (error) {
            setOtpError(error.message);
          }
          setIsLoading(false);
        });
      }
    });
    // signIn();
  };

  useEffect(() => {
    SplashScreen.hide();
    console.log('params', otpInput.current);
    if (Platform.OS === 'android') {
      if (height > 800) {
        setScrollHeight(height + 45);
        return;
      }
      setScrollHeight(height - 2);
    } else {
      setScrollHeight(height - 70);
    }
  }, []);

  const getVerticalOffSet = () => {
    if (Platform.OS === 'android') {
      if (height > 800) {
        return -300;
      }
      return -220;
    } else {
      return 40;
    }
  };
  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={getVerticalOffSet()}
      style={{flex: 1}}
      enabled
      keyboardShouldPersistTaps={true}
      behavior="padding">
      <ScrollView
        contentContainerStyle={{
          height: scrollHeight,
        }}
        bounces={false}>
        <View style={{flex: 1, backgroundColor: Colors.white}}>
          <View style={{flex: 0.3}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'cover'}}
                source={ImageUrl.brandLogo}
              />
            </View>
            <View style={{alignItems: 'center'}}>
              <Text
                style={{
                  color: Colors.secondryColor,
                  fontSize: Fonts.Text,
                }}>
                We have sent{' '}
                <Text style={{color: Colors.primaryColor}}>OTP</Text> to
                register Mobile or Email
              </Text>
            </View>
          </View>

          <View
            style={{
              flex: 0.5,
              justifyContent: 'center',
              marginHorizontal: 20,
            }}>
            <View
              style={{
                flexDirection: 'column',
                marginBottom: 20,
                justifyContent: 'space-between',
              }}>
              <OTPTextInput
                inputCount={6}
                tintColor={Colors.primaryColor}
                offTintColor={Colors.secondryColor}
                ref={e => (otpInput = e)}
                handleTextChange={otp => {
                  setOtp(otp);
                }}
              />
            </View>
            <HelperText
              type="error"
              style={{textAlign: 'center', fontSize: Fonts.Text}}
              visible={otpError !== ''}>
              {otpError}
            </HelperText>
            <TouchableOpacity
              style={{
                height: 40,
                // width: '30%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.primaryColor,
                borderRadius: 25,
                marginBottom: 15,
              }}
              onPress={onVerifyOTP}>
              {isLoading ? (
                <ActivityIndicator color="#D7962E" size="large" />
              ) : (
                <Text
                  style={{
                    color: Colors.white,
                    fontSize: Fonts.subHeading,
                    // fontFamily: 'roboto-regular',
                  }}>
                  Verify
                </Text>
              )}
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 0.2,
              justifyContent: 'flex-end',
              paddingBottom: 20,
              alignItems: 'center',
            }}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
              // onPress={() => navigation.navigate('Registration')}
              >
                <Text
                  style={{
                    color: Colors.primaryColor,
                    // fontFamily: 'roboto-regular',
                  }}>
                  Resend OTP
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}
