import React, { useContext, useState, useEffect, useMemo } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { Avatar, HelperText } from 'react-native-paper';

import { ImageUrl } from '../../utils';
import { Colors, Fonts } from '../../theme';
import { AuthContext } from '../../navigation/index';
import api from '../../api';
import { useSelector, useDispatch } from 'react-redux';
export function Registration({ navigation }) {
  const { signUp } = useContext(AuthContext);
  const SCREEN_HEIGHT = Dimensions.get('window').height;
  const SCREEN_WIDTH = Dimensions.get('window').width;
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [scrollHeight, setScrollHeight] = useState(SCREEN_HEIGHT);
  const [isLoading, setIsLoading] = useState(false);
  const registrationError = useSelector(
    (store) => store.authReducer.registrationError
  );
  const Dispatch = useDispatch();
  console.log('registrationError', registrationError);
  const onSignUp = () => {
    if (
      !firstName ||
      !lastName ||
      !email ||
      !phoneNumber ||
      !password ||
      !confirmPassword
    ) {
      alert('Please file all details');
      return;
    }
    if (password !== confirmPassword) {
      alert('Password and confirm password not matched');
      return;
    }
    setIsLoading(true);
    let data = {
      first_name: firstName,
      last_name: lastName,
      mobile_number: phoneNumber,
      email: email,
      password: password,
      re_password: confirmPassword,
    };
    api.auth.registration(data).then((res) => {
      if (res.status === 201) {
        res.json().then((response) => {
          signUp(response);
          navigation.navigate('Otp', response);
          setIsLoading(false);
        });
      }
      if (res.status === 400) {
        alert('Registration Failed! Please try again');
        res.json().then((error) => {
          setIsLoading(false);
          Dispatch(registration_error(error));
        });
      }
    });
  };
  useEffect(() => {}, [registrationError]);
  useEffect(() => {
    if (Platform.OS === 'android') {
      if (SCREEN_HEIGHT > 800) {
        setScrollHeight(SCREEN_HEIGHT + 45);
        return;
      }

      setScrollHeight(SCREEN_HEIGHT - 2);
    } else {
      setScrollHeight(SCREEN_HEIGHT - 65);
    }
  }, []);

  const getVerticalOffSet = () => {
    if (Platform.OS === 'android') {
      if (SCREEN_HEIGHT > 800) {
        return -345;
      }
      return -265;
    } else {
      return 60;
    }
  };

  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={getVerticalOffSet()}
      style={{ flex: 1 }}
      enabled
      keyboardShouldPersistTaps={true}
      behavior="padding"
    >
      <ScrollView
        contentContainerStyle={{
          height: scrollHeight,
        }}
      >
        <View style={{ flex: 1, backgroundColor: Colors.white }}>
          <View style={{ flex: 0.3 }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Image
                style={{ height: '100%', width: '100%', resizeMode: 'cover' }}
                source={ImageUrl.brandLogo}
              />
            </View>
          </View>
          <View
            style={{
              flex: 0.5,
              justifyContent: 'center',
              marginHorizontal: 20,
            }}
          >
            <HelperText
              type="error"
              style={{ textAlign: 'center', fontSize: Fonts.Text }}
              visible={registrationError.non_field_errors === undefined}
            >
              {registrationError.non_field_errors}
            </HelperText>
            <TextInput
              style={{
                height: 40,
                borderColor: Colors.primaryColor,
                borderWidth: 1,
                marginBottom: 0,
                borderRadius: 20,
                padding: 10,
              }}
              onChangeText={(text) => setFirstName(text)}
              value={firstName}
              placeholder={'First Name'}
            />
            <HelperText
              type="error"
              visible={registrationError.first_name !== undefined}
            >
              {registrationError.first_name}
            </HelperText>
            <TextInput
              style={{
                height: 40,
                borderColor: Colors.primaryColor,
                borderWidth: 1,
                marginBottom: 0,
                borderRadius: 20,
                padding: 10,
              }}
              onChangeText={(text) => setLastName(text)}
              value={lastName}
              placeholder={'Last Name'}
            />
            <HelperText
              type="error"
              visible={registrationError.last_name !== undefined}
            >
              {registrationError.last_name}
            </HelperText>
            <TextInput
              style={{
                height: 40,
                borderColor: Colors.primaryColor,
                borderWidth: 1,
                marginBottom: 0,
                borderRadius: 20,
                padding: 10,
              }}
              onChangeText={(text) => setEmail(text)}
              value={email}
              placeholder={'Email'}
            />
            <HelperText
              type="error"
              visible={registrationError.email !== undefined}
            >
              {registrationError.email}
            </HelperText>
            <TextInput
              style={{
                height: 40,
                borderColor: Colors.primaryColor,
                borderWidth: 1,
                marginBottom: 0,
                borderRadius: 20,
                padding: 10,
              }}
              onChangeText={(text) => setPhoneNumber(text)}
              value={phoneNumber}
              placeholder={'Phone Number'}
              keyboardType={'numeric'}
            />
            <HelperText
              type="error"
              visible={registrationError.mobile_number !== undefined}
            >
              {registrationError.mobile_number}
            </HelperText>
            <TextInput
              style={{
                height: 40,
                borderColor: Colors.primaryColor,
                borderWidth: 1,
                marginBottom: 0,
                borderRadius: 20,
                padding: 10,
              }}
              onChangeText={(text) => setPassword(text)}
              value={password}
              placeholder={'Password'}
              secureTextEntry
            />
            <HelperText
              type="error"
              visible={registrationError.password !== undefined}
            >
              {registrationError.password}
            </HelperText>
            <TextInput
              style={{
                height: 40,
                borderColor: Colors.primaryColor,
                borderWidth: 1,
                marginBottom: 0,
                borderRadius: 20,
                padding: 10,
              }}
              onChangeText={(text) => setConfirmPassword(text)}
              value={confirmPassword}
              placeholder={'Confirm Password'}
              secureTextEntry
            />
            <HelperText
              type="error"
              visible={registrationError.re_password !== undefined}
            >
              {registrationError.re_password}
            </HelperText>
            <TouchableOpacity
              style={{
                height: 40,
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.primaryColor,
                borderRadius: 25,
                marginBottom: 15,
              }}
              onPress={() => onSignUp()}
            >
              {isLoading ? (
                <ActivityIndicator color="#D7962E" size="large" />
              ) : (
                <Text
                  style={{
                    color: Colors.white,
                    fontSize: Fonts.subHeading,
                    // fontFamily: 'roboto-regular',
                  }}
                >
                  Registration
                </Text>
              )}
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 0.2,
              justifyContent: 'flex-end',
              paddingBottom: 20,
              alignItems: 'center',
            }}
          >
            <View style={{ flexDirection: 'row' }}>
              <Text
                style={{
                  // fontFamily: 'roboto-regular',
                  color: Colors.secondryColor,
                }}
              >
                Already have an account ?{' '}
              </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <Text
                  style={{
                    color: Colors.primaryColor,
                    // fontFamily: 'roboto-regular',
                  }}
                >
                  Sign In
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}
