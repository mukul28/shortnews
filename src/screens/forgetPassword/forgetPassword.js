import React, { useContext, useState, useEffect } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';

import { ImageUrl } from '../../utils';
import { Colors, Fonts } from '../../theme';
import { AuthContext } from '../../navigation/index';

export function ForgetPassword({ navigation }) {
  const { signIn } = useContext(AuthContext);
  const SCREEN_HEIGHT = Dimensions.get('window').height;
  const SCREEN_WIDTH = Dimensions.get('window').width;
  const [scrollHeight, setScrollHeight] = useState(SCREEN_HEIGHT);
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  const onSignIn = () => {
    signIn();
  };

  useEffect(() => {
    if (Platform.OS === 'android') {
      if (SCREEN_HEIGHT > 800) {
        setScrollHeight(SCREEN_HEIGHT + 45);
        return;
      }

      setScrollHeight(SCREEN_HEIGHT - 2);
    } else {
      setScrollHeight(SCREEN_HEIGHT - 70);
    }
  }, []);

  const getVerticalOffSet = () => {
    if (Platform.OS === 'android') {
      if (SCREEN_HEIGHT > 800) {
        return -345;
      }
      return -265;
    } else {
      return 40;
    }
  };

  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={getVerticalOffSet()}
      style={{ flex: 1 }}
      enabled
      keyboardShouldPersistTaps={true}
      behavior="padding"
    >
      <ScrollView
        contentContainerStyle={{
          height: scrollHeight,
        }}
        bounces={false}
      >
        <View style={{ flex: 1, backgroundColor: Colors.white }}>
          <View style={{ flex: 0.3 }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Image
                style={{ height: '100%', width: '100%', resizeMode: 'cover' }}
                source={ImageUrl.brandLogo}
              />
            </View>
          </View>
          <View
            style={{
              flex: 0.5,
              justifyContent: 'center',
              marginHorizontal: 20,
            }}
          >
            <TextInput
              style={{
                height: 40,
                borderColor: Colors.primaryColor,
                borderWidth: 1,
                marginBottom: 15,
                borderRadius: 20,
                padding: 10,
              }}
              onChangeText={(text) => setEmail(text)}
              value={email}
              placeholder={'Email'}
            />
            {/* <Text style={{ alignSelf: 'center', marginBottom: 15 }}>OR</Text>
            <TextInput
              style={{
                height: 40,
                borderColor: Colors.primaryColor,
                borderWidth: 1,
                marginBottom: 15,
                borderRadius: 20,
                padding: 10,
              }}
              onChangeText={(text) => setPhoneNumber(text)}
              value={phoneNumber}
              placeholder={'Phone Number'}
              keyboardType={'numeric'}
            /> */}
            <TouchableOpacity
              style={{
                height: 40,
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.primaryColor,
                borderRadius: 25,
                marginBottom: 15,
              }}
              onPress={() => navigation.navigate('Otp')}
            >
              <Text
                style={{
                  color: Colors.white,
                  fontSize: Fonts.subHeading,
                  // fontFamily: 'roboto-regular',
                }}
              >
                Send OTP
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 0.2,
              justifyContent: 'flex-end',
              paddingBottom: 20,
              alignItems: 'center',
            }}
          >
            <View style={{ flexDirection: 'row' }}>
              <Text
                style={{
                  // fontFamily: 'roboto-regular',
                  color: Colors.secondryColor,
                }}
              >
                Back to{' '}
              </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <Text
                  style={{
                    color: Colors.primaryColor,
                    // fontFamily: 'roboto-regular',
                  }}
                >
                  Sign In
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}
