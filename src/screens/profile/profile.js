import React, { useState, useEffect, useContext } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicon from 'react-native-vector-icons/Ionicons';

import { ImageUrl } from '../../utils';
import { Colors, Fonts } from '../../theme';
import { Headline, List, Divider } from 'react-native-paper';
import { AuthContext } from '../../navigation/index';
import { useSelector } from 'react-redux';

export function Profile({ navigation }) {
  const { signOut } = useContext(AuthContext);
  const SCREEN_HEIGHT = Dimensions.get('window').height;
  const SCREEN_WIDTH = Dimensions.get('window').width;
  const [scrollHeight, setScrollHeight] = useState(SCREEN_HEIGHT);
  const [isLoading, setIsLoading] = useState(false);

  const user = useSelector((store) => store.userReducer.user);
  const onLogout = () => {
    setIsLoading(true);
    signOut();
  };

  useEffect(() => {
    if (Platform.OS === 'android') {
      if (SCREEN_HEIGHT > 800) {
        setScrollHeight(SCREEN_HEIGHT + 45);
        return;
      }

      setScrollHeight(SCREEN_HEIGHT - 2);
    } else {
      setScrollHeight(SCREEN_HEIGHT - 70);
    }
  }, []);

  const getVerticalOffSet = () => {
    if (Platform.OS === 'android') {
      if (SCREEN_HEIGHT > 800) {
        return -300;
      }
      return -220;
    } else {
      return 40;
    }
  };
  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={getVerticalOffSet()}
      style={{ flex: 1 }}
      enabled
      keyboardShouldPersistTaps={true}
      behavior="padding"
    >
      <ScrollView
        contentContainerStyle={{
          height: scrollHeight,
        }}
        bounces={false}
      >
        <View style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              // position: 'absolute',
              height: SCREEN_HEIGHT,
              width: SCREEN_WIDTH,
              backgroundColor: 'white',
            }}
          >
            <View
              style={{
                flex: 0.08,
                flexDirection: 'row',
                borderBottomWidth: 0,
                borderBottomColor: Colors.primaryColor,
              }}
            >
              <TouchableOpacity
                style={{
                  flex: 0.2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => {
                  navigation.goBack();
                }}
              >
                <Ionicon
                  name="ios-arrow-back"
                  size={25}
                  color={Colors.primaryColor}
                />
              </TouchableOpacity>
              <View
                style={{
                  flex: 0.6,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Image
                  source={ImageUrl.brandLogo}
                  style={{
                    height: '100%',
                    width: '100%',
                    resizeMode: 'cover',
                  }}
                />
              </View>
              <TouchableOpacity
                style={{
                  flex: 0.2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => navigation.navigate('EditProfile')}
              >
                <FontAwesome
                  name="pencil"
                  size={25}
                  color={Colors.primaryColor}
                />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 0.92 }}>
              <View
                style={{
                  flex: 0.2,
                  backgroundColor: '#fff',
                  borderWidth: 3,
                  borderTopWidth: 0,
                  // borderStyle: 'dotted',
                  borderBottomLeftRadius: 70,
                  borderBottomRightRadius: 70,
                  borderColor: Colors.primaryColor,
                }}
              />
              <View
                style={{
                  flex: 0.8,
                  backgroundColor: '#fff',
                  marginTop: 5,
                  borderWidth: 3,
                  // borderStyle: 'dotted',
                  borderBottomWidth: 0,
                  borderTopLeftRadius: 70,
                  borderTopRightRadius: 70,
                  borderColor: Colors.primaryColor,
                }}
              >
                <View style={{ flex: 0.25, justifyContent: 'flex-end' }}>
                  <Headline
                    style={{ alignSelf: 'center', color: Colors.primaryColor }}
                  >
                    {user.display_name}
                  </Headline>
                </View>
                <View style={{ flex: 0.63, marginHorizontal: 10 }}>
                  <List.Item
                    title="First Name"
                    description={user.first_name}
                    left={(props) => (
                      <FontAwesome
                        style={{
                          alignSelf: 'center',
                          paddingRight: 15,
                          color: Colors.primaryColor,
                        }}
                        size={30}
                        name="user"
                      />
                    )}
                  />
                  <Divider style={{ backgroundColor: Colors.primaryColor }} />
                  <List.Item
                    title="Last Name"
                    description={user.last_name}
                    left={(props) => (
                      <FontAwesome
                        style={{
                          alignSelf: 'center',
                          paddingRight: 15,
                          color: Colors.primaryColor,
                        }}
                        size={30}
                        name="user-o"
                      />
                    )}
                  />
                  <Divider style={{ backgroundColor: Colors.primaryColor }} />
                  <List.Item
                    title="Email"
                    description={user.email}
                    left={(props) => (
                      <MaterialCommunityIcons
                        style={{
                          alignSelf: 'center',
                          paddingRight: 15,
                          color: Colors.primaryColor,
                        }}
                        size={30}
                        name="email-check-outline"
                      />
                    )}
                  />
                  <Divider style={{ backgroundColor: Colors.primaryColor }} />
                  <List.Item
                    title="Contact Number"
                    description={
                      user.mobile_number ? user.mobile_number : '+91 1234543561'
                    }
                    left={(props) => (
                      <FontAwesome
                        style={{
                          alignSelf: 'center',
                          paddingRight: 15,
                          color: Colors.primaryColor,
                        }}
                        size={30}
                        name="phone"
                      />
                    )}
                  />
                  <Divider style={{ backgroundColor: Colors.primaryColor }} />
                </View>
                <TouchableOpacity
                  style={{
                    flex: 0.12,
                    // marginHorizontal: 10,
                    backgroundColor: Colors.primaryColor,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={isLoading ? () => {} : onLogout}
                >
                  {isLoading ? (
                    <ActivityIndicator color="#D7962E" size="large" />
                  ) : (
                    <Text
                      style={{
                        color: Colors.white,
                        fontSize: Fonts.subHeading,
                      }}
                    >
                      Logout
                    </Text>
                  )}
                </TouchableOpacity>
              </View>
              <Image
                source={
                  user.profile_pic
                    ? { uri: user.profile_pic }
                    : ImageUrl.profileLogo
                }
                style={{
                  flex: 1,
                  height: '23%',
                  width: '41%',
                  borderRadius: 100,
                  alignSelf: 'center',
                  position: 'absolute',
                  top: '9%',
                  borderColor: Colors.primaryColor,
                  borderWidth: 8,
                }}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}
