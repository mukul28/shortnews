import React, {useContext, useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Dimensions,
  Platform,
  ActivityIndicator,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';

import {AuthContext} from '../../navigation/index';
import {ImageUrl} from '../../utils';
import {Colors, Fonts} from '../../theme';
import api from '../../api';
import {useDispatch} from 'react-redux';
import {login_error} from '../../redux/actions';
export function Login({navigation}) {
  const {signIn} = useContext(AuthContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const {height} = Dimensions.get('window');
  const [scrollHeight, setScrollHeight] = useState(height);
  const [isLoading, setIsLoading] = useState(false);
  const Dispatch = useDispatch();
  const onSignIn = () => {
    if (!email || !password) {
      alert('Please file all details');
      return;
    }
    setIsLoading(true);
    let data = {email, password};
    api.auth.login(data).then(res => {
      debugger;
      if (res.status === 200) {
        res.json().then(async response => {
          setIsLoading(false);
          signIn(response);
        });
      }
      if (res.status === 400) {
        alert('Login Failed!');
        res.json().then(response => {
          setIsLoading(false);
          Dispatch(login_error(response.errors));
          return false;
        });
      }
    });
  };

  useEffect(() => {
    // SplashScreen.hide();
    GoogleSignin.configure({
      webClientId:
        '694654394260-r9okb6c327fk7p2r9oh3e6p0spbc9r0p.apps.googleusercontent.com',
    });
    if (Platform.OS === 'android') {
      if (height > 800) {
        setScrollHeight(height + 45);
        return;
      }

      setScrollHeight(height - 2);
    } else {
      setScrollHeight(height - 70);
    }
  }, []);

  const getVerticalOffSet = () => {
    if (Platform.OS === 'android') {
      if (height > 800) {
        return -300;
      }
      return -220;
    } else {
      return 40;
    }
  };

  const onFacbokLogin = async () => {
    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ])
      .then(obj => {})
      .catch(err => {});

    if (result.isCancelled) {
      throw 'User cancelled the login process';
    }

    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();

    if (!data) {
      throw 'Something went wrong obtaining access token';
    }
    console.log('data.accessToken===========', data);
    const infoRequest = new GraphRequest(
      '/me?fields=name,email,first_name,last_name,picture',
      null,
      _responseInfoCallback,
    );
    new GraphRequestManager().addRequest(infoRequest).start();

    // Create a Firebase credential with the AccessToken
    return;
  };

  _responseInfoCallback = (error, result) => {
    if (error) {
      alert('Error fetching data: ' + error.toString());
    } else {
      setIsLoading(true);
      let data = {
        user: {
          email: result.email,
          familyName: result.last_name,
          givenName: result.first_name,
          id: result.id,
          name: result.name,
          photo: result.picture.data.url,
          facebook_google: 1,
        },
      };
      api.auth
        .socialLogin(data)
        .then(res => {
          if (res.status === 200) {
            res.json().then(async response => {
              setIsLoading(false);
              signIn(response);
            });
          }
          if (res.status === 400) {
            alert('Login Failed!');
            res.json().then(response => {
              setIsLoading(false);
              Dispatch(login_error(response.errors));
              return false;
            });
          }
        })
        .catch(error => {
          setIsLoading(false);
          console.log('_responseInfoCallback -> error', error);
        });
    }
  };

  const onGoogleLogin = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      setIsLoading(true);
      let data = {
        user: {
          email: userInfo.user.email,
          familyName: userInfo.user.familyName,
          givenName: userInfo.user.givenName,
          id: userInfo.user.id,
          name: userInfo.user.name,
          photo: userInfo.user.photo,
          facebook_google: 0,
        },
      };
      api.auth
        .socialLogin(data)
        .then(res => {
          if (res.status === 200) {
            res.json().then(async response => {
              setIsLoading(false);
              signIn(response);
            });
          }
          if (res.status === 400) {
            alert('Login Failed!');
            res.json().then(response => {
              setIsLoading(false);
              Dispatch(login_error(response.errors));
              return false;
            });
          }
        })
        .catch(error => {
          setIsLoading(false);
          console.log('onGoogleLogin -> error', error);
        });
    } catch (error) {
      console.log('onGoogleLogin -> error', error);
      // alert(error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // alert('user cancelled the login flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // alert('operation (f.e. sign in) is in progress already');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // alert('play services not available or outdated');
      } else {
        // alert('some other error happened');
      }
    }
  };

  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={getVerticalOffSet()}
      style={{flex: 1}}
      enabled
      keyboardShouldPersistTaps={true}
      behavior="padding">
      <ScrollView
        contentContainerStyle={{
          height: scrollHeight,
        }}
        bounces={false}>
        <View style={{flex: 1, backgroundColor: Colors.white}}>
          <View style={{flex: 0.3}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'cover'}}
                source={ImageUrl.brandLogo}
              />
            </View>
          </View>
          <View
            style={{
              flex: 0.5,
              justifyContent: 'center',
              marginHorizontal: 20,
            }}>
            <TextInput
              style={{
                height: 40,
                borderColor: Colors.primaryColor,
                borderWidth: 1,
                marginBottom: 15,
                borderRadius: 20,
                padding: 10,
              }}
              onChangeText={text => setEmail(text)}
              value={email}
              placeholder={'User name'}
            />
            <TextInput
              style={{
                height: 40,
                borderColor: Colors.primaryColor,
                borderWidth: 1,
                marginBottom: 15,
                borderRadius: 20,
                padding: 10,
              }}
              onChangeText={text => setPassword(text)}
              value={password}
              placeholder={'Password'}
              secureTextEntry
            />
            <TouchableOpacity
              style={{
                height: 40,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.primaryColor,
                borderRadius: 25,
                marginBottom: 15,
              }}
              onPress={isLoading ? () => {} : onSignIn}>
              {isLoading ? (
                <ActivityIndicator color="#D7962E" size="large" />
              ) : (
                <Text
                  style={{
                    color: Colors.white,
                    fontSize: Fonts.subHeading,
                    // fontFamily: 'roboto-regular',
                  }}>
                  Login
                </Text>
              )}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('ForgetPassword')}
              style={{alignSelf: 'center', marginBottom: 10}}>
              <Text
                style={{
                  color: Colors.primaryColor,
                  // fontFamily: 'roboto-regular',
                }}>
                Forget Password?
              </Text>
            </TouchableOpacity>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text
                style={{
                  color: Colors.secondryColor,
                  // fontFamily: 'roboto-regular',
                }}>
                login with
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: 20,
                }}>
                <TouchableOpacity
                  style={{
                    height: 50,
                    width: 50,
                    borderRadius: 25,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#45619D',
                    marginHorizontal: 10,
                  }}
                  onPress={() => onFacbokLogin()}>
                  <FontAwesome
                    name="facebook-f"
                    size={30}
                    color={Colors.white}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    height: 50,
                    width: 50,
                    borderRadius: 25,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#F3511E',
                    marginHorizontal: 10,
                  }}
                  onPress={onGoogleLogin}>
                  <FontAwesome
                    name="google-plus"
                    size={30}
                    color={Colors.white}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View
            style={{
              flex: 0.2,
              justifyContent: 'flex-end',
              paddingBottom: 20,
              alignItems: 'center',
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  // fontFamily: 'roboto-regular',
                  color: Colors.secondryColor,
                }}>
                Don't have an account?{' '}
              </Text>
              <TouchableOpacity
                onPress={() => navigation.navigate('Registration')}>
                <Text
                  style={{
                    color: Colors.primaryColor,
                    // fontFamily: 'roboto-regular',
                  }}>
                  Sign up here
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}
