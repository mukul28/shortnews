import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useSelector, useDispatch} from 'react-redux';
import Ionicon from 'react-native-vector-icons/Ionicons';
import {Headline, List, Divider} from 'react-native-paper';

import {ImageUrl} from '../../utils';
import {Colors, Fonts} from '../../theme';
import api from '../../api';
import {updateUser} from '../../redux/actions';

export function EditProfile({navigation}) {
  const SCREEN_HEIGHT = Dimensions.get('window').height;
  const SCREEN_WIDTH = Dimensions.get('window').width;
  const {user, accessToken} = useSelector(store => store.userReducer);
  console.log(
    'Profile -> userReducer 124323453427654365783246579823652937',
    user,
    accessToken,
  );

  const [scrollHeight, setScrollHeight] = useState(SCREEN_HEIGHT);
  // const [displayName, setDisplayName] = useState('');
  const [firstName, setFirstName] = useState(user.first_name);
  const [lastName, setLastName] = useState(user.last_name);
  const [email, setEmail] = useState(user.email);
  const [phoneNumber, setPhoneNumber] = useState(user.mobile_number);
  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    if (Platform.OS === 'android') {
      if (SCREEN_HEIGHT > 800) {
        setScrollHeight(SCREEN_HEIGHT + 45);
        return;
      }

      setScrollHeight(SCREEN_HEIGHT - 2);
    } else {
      setScrollHeight(SCREEN_HEIGHT - 70);
    }
  }, []);

  const onSave = () => {
    setIsLoading(true);
    if (!firstName || !lastName || !phoneNumber) {
      alert('Please file all details');
      return;
    }

    setIsLoading(true);
    const data = {
      first_name: firstName,
      last_name: lastName,
      mobile_number: phoneNumber,
    };
    console.log(data, 'sdasdasdasda');
    api.auth
      .updateProfile(accessToken, data)
      .then(res => {
        if (res.status === 200) {
          res.json().then(async response => {
            dispatch(updateUser(response));

            setIsLoading(false);
            navigation.navigate('Profile');
          });
        }
        if (res.status === 400) {
          alert('Something went wrong please try again');
          res.json().then(response => {
            setIsLoading(false);
          });
        }
      })
      .catch(error => {
        console.log('onSave -> error', error);
        setIsLoading(false);
      });
  };

  const getVerticalOffSet = () => {
    if (Platform.OS === 'android') {
      if (SCREEN_HEIGHT > 800) {
        return -345;
      }
      return -265;
    } else {
      return 40;
    }
  };
  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={getVerticalOffSet()}
      style={{flex: 1}}
      enabled
      keyboardShouldPersistTaps={true}
      behavior="padding">
      <ScrollView
        contentContainerStyle={{
          height: scrollHeight,
        }}
        bounces={false}>
        <View style={{flex: 1}}>
          <View
            style={{
              flex: 1,
              // position: 'absolute',
              height: SCREEN_HEIGHT,
              width: SCREEN_WIDTH,
              backgroundColor: 'white',
            }}>
            <View
              style={{
                flex: 0.08,
                flexDirection: 'row',
                borderBottomWidth: 0,
                borderBottomColor: Colors.primaryColor,
              }}>
              <TouchableOpacity
                style={{
                  flex: 0.2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => {
                  navigation.goBack();
                }}>
                <Ionicon
                  name="ios-arrow-back"
                  size={25}
                  color={Colors.primaryColor}
                />
              </TouchableOpacity>
              <View
                style={{
                  flex: 0.6,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={ImageUrl.brandLogo}
                  style={{
                    height: '100%',
                    width: '100%',
                    resizeMode: 'cover',
                  }}
                />
              </View>
              <TouchableOpacity
                style={{
                  flex: 0.2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={isLoading ? () => {} : onSave}>
                {/* <FontAwesome
                  name="pencil"
                  size={25}
                  color={Colors.primaryColor}
                /> */}
                {isLoading ? (
                  <ActivityIndicator color="#D7962E" size="large" />
                ) : (
                  <Text
                    style={{
                      color: Colors.primaryColor,
                      fontSize: Fonts.textLarge,
                      fontWeight: 'bold',
                    }}>
                    Save
                  </Text>
                )}
              </TouchableOpacity>
            </View>
            <View style={{flex: 0.92}}>
              <View
                style={{
                  flex: 0.2,
                  backgroundColor: '#fff',
                  borderWidth: 3,
                  borderTopWidth: 0,
                  // borderStyle: 'dotted',
                  borderBottomLeftRadius: 70,
                  borderBottomRightRadius: 70,
                  borderColor: Colors.primaryColor,
                }}
              />
              <View
                style={{
                  flex: 0.8,
                  backgroundColor: '#fff',
                  marginTop: 5,
                  borderWidth: 3,
                  // borderStyle: 'dotted',
                  borderBottomWidth: 0,
                  borderTopLeftRadius: 70,
                  borderTopRightRadius: 70,
                  borderColor: Colors.primaryColor,
                }}>
                <View
                  style={{
                    flex: 0.25,
                    justifyContent: 'flex-end',
                    paddingBottom: 10,
                  }}>
                  <Headline
                    style={{alignSelf: 'center', color: Colors.primaryColor}}>
                    Edit Profile
                  </Headline>
                </View>
                <View style={{flex: 0.75, marginHorizontal: 10}}>
                  {/* <TextInput
                    style={{
                      height: 40,
                      borderColor: Colors.primaryColor,
                      borderWidth: 1,
                      marginBottom: 15,
                      borderRadius: 20,
                      padding: 10,
                    }}
                    onChangeText={(text) => setDisplayName(text)}
                    value={displayName}
                    placeholder={'Display Name'}
                  /> */}
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: Colors.primaryColor,
                      borderWidth: 1,
                      marginBottom: 15,
                      borderRadius: 20,
                      padding: 10,
                    }}
                    onChangeText={text => setFirstName(text)}
                    value={firstName}
                    placeholder={'First Name'}
                  />
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: Colors.primaryColor,
                      borderWidth: 1,
                      marginBottom: 15,
                      borderRadius: 20,
                      padding: 10,
                    }}
                    onChangeText={text => setLastName(text)}
                    value={lastName}
                    placeholder={'Last Name'}
                  />
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: Colors.primaryColor,
                      borderWidth: 1,
                      marginBottom: 15,
                      borderRadius: 20,
                      padding: 10,
                    }}
                    editable={false}
                    // onChangeText={(text) => setEmail(text)}
                    value={email}
                    // placeholder={'Email'}
                  />

                  <TextInput
                    style={{
                      height: 40,
                      borderColor: Colors.primaryColor,
                      borderWidth: 1,
                      marginBottom: 15,
                      borderRadius: 20,
                      padding: 10,
                    }}
                    onChangeText={text => setPhoneNumber(text)}
                    value={phoneNumber}
                    placeholder={'Phone Number'}
                    keyboardType={'numeric'}
                  />
                </View>
              </View>
              <View
                style={{
                  height: '23%',
                  width: '41%',
                  borderRadius: 100,
                  alignSelf: 'center',
                  position: 'absolute',
                  top: '9%',
                  borderColor: Colors.primaryColor,
                  borderWidth: 4,
                }}>
                <Image
                  source={
                    user.profile_pic
                      ? {uri: user.profile_pic}
                      : ImageUrl.profileLogo
                  }
                  style={{
                    height: '100%',
                    width: '100%',
                    borderRadius: 100,
                    // alignSelf: 'center',
                    // position: 'absolute',
                    // top: '9%',
                    borderColor: Colors.primaryColor,
                    borderWidth: 4,
                  }}
                />
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}
