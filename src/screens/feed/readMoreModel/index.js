import React, {useContext, useState, Fragment} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Text,
  Dimensions,
} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import {Modal, Paragraph, Headline} from 'react-native-paper';
import {Colors, Fonts} from '../../../theme';
import {ImageUrl} from '../../../utils';
import {SliderBox} from 'react-native-image-slider-box';
import _ from 'lodash';
export function ReadMoreModel({isVisible, _hideModal, content}) {
  const renderArticle = () => {
    if (content.extraImagesArray) {
      if (content.extraImagesArray.length === 1) {
        return (
          <Image
            source={{uri: content.images[0].file}}
            style={{
              flex: 1,
              height: null,
              width: null,
              resizeMode: 'cover',
            }}
          />
        );
      } else {
        return (
          <SliderBox
            images={content.extraImagesArray}
            dotColor={Colors.primaryColor}
            inactiveDotColor={Colors.secondryColor}
            sliderBoxHeight={'100%'}
            resizeMode={'contain'}
            resizeMethod={'resize'}
            circleLoop
          />
        );
      }
    }
    return null;
  };
  if (content === null) return null;
  return (
    <Modal
      visible={isVisible}
      onDismiss={_hideModal}
      contentContainerStyle={{flex: 1, justifyContent: 'flex-end'}}>
      <View style={{flex: 0.3}} />
      <View
        style={{
          flex: 9.7,
          backgroundColor: '#fff',
          justifyContent: 'space-between',
          borderTopStartRadius: 20,
          borderTopEndRadius: 20,
          paddingHorizontal: 16,
        }}>
        <View
          style={{
            flex: 0,
            justifyContent: 'space-between',
            flexDirection: 'row',
            marginVertical: 20,
          }}>
          <TouchableOpacity
            style={{width: 100, paddingLeft: 10}}
            onPress={_hideModal}>
            <Ionicon
              name="ios-arrow-back"
              size={30}
              color={Colors.primaryColor}
            />
          </TouchableOpacity>
          <Image
            source={ImageUrl.brandLogo}
            style={{
              height: '100%',
              width: '40%',
            }}
          />
        </View>
        <View style={{flex: 1}}>
          <View style={{}}>
            <Text
              style={{
                fontSize: Fonts.subHeading,
                fontWeight: 'bold',
                alignSelf: 'center',
                maxWidth: '95%',
                textAlign: 'justify',
              }}>
              {content.title}
            </Text>
          </View>
          <View
            style={{
              flex: 0.45,
              borderWidth: 1,
              marginTop: 10,
              borderColor: Colors.primaryColor,
            }}>
            {renderArticle()}
          </View>
          <View style={{flex: 0.65, paddingTop: 20}}>
            <ScrollView>
              <Paragraph
                style={{
                  alignSelf: 'center',
                  maxWidth: '95%',
                  textAlign: 'justify',
                }}>
                {content.description}
              </Paragraph>
            </ScrollView>
          </View>
        </View>
      </View>
    </Modal>
  );
}
