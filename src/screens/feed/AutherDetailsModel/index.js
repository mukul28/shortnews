import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {Modal, Headline, Paragraph} from 'react-native-paper';
import {Colors} from '../../../theme';
import {ImageUrl} from '../../../utils';

export function AutherDetailsModel({isVisible, _hideModal, content}) {
  const regex = /(<([^>]+)>)/gi;
  return (
    <Modal
      visible={isVisible}
      onDismiss={_hideModal}
      contentContainerStyle={{flex: 1, justifyContent: 'flex-end'}}>
      <TouchableOpacity style={{flex: 0.6}} onPress={_hideModal} />
      <View
        style={{
          flex: 0.4,
          backgroundColor: Colors.white,
          borderTopStartRadius: 20,
          borderTopEndRadius: 20,
        }}>
        <View
          style={{
            flex: 0.3,
            borderTopStartRadius: 20,
            borderTopEndRadius: 20,
            justifyContent: 'center',
            backgroundColor: Colors.primaryColor,
          }}>
          <Image
            source={{uri: content.image}}
            style={{
              height: 120,
              width: 120,
              borderRadius: 100,
              alignSelf: 'center',
              position: 'relative',
              top: '50%',
              bottom: '20%',
              borderColor: Colors.primaryColor,
              borderWidth: 2,
              backgroundColor: Colors.primaryColor,
            }}
          />
        </View>
        <View
          style={{
            justifyContent: 'flex-start',
            flex: 0.7,
            borderTopRadius: 20,
            alignContent: 'center',
            top: '25%',
          }}>
          <Headline
            style={{
              alignSelf: 'center',
              fontWeight: 'bold',

              // fontFamily: 'roboto-regular'
            }}>
            {content.name}
          </Headline>

          <Paragraph
            style={{
              alignSelf: 'center',
              maxWidth: '90%',
              textAlign: 'justify',
              // fontFamily: 'roboto-regular',
            }}>
            {content.description}
          </Paragraph>
        </View>
      </View>
    </Modal>
  );
}
