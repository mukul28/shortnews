import React, {useState, Fragment, useRef, useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  Animated,
  BackHandler,
  Alert,
} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Swiper from 'react-native-swiper';
import {ImageUrl} from '../../utils';
import {Colors, Fonts} from '../../theme';
import {ReadMoreModel} from './readMoreModel';
import {AutherDetailsModel} from './AutherDetailsModel';
import {useSelector, useDispatch} from 'react-redux';
import {appendFeeds} from '../../redux/actions';
import _ from 'lodash';
import api from '../../api';
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

export function Feed({navigation}) {
  const [isHeader, setIsHeader] = useState(false);
  const [isAnimation, setIsAnimation] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const [isAutherShow, setIsAutherShow] = useState(false);
  const [readMoreData, setReadMoreData] = useState(null);
  const [autherData, setAutherData] = useState();
  const dispatch = useDispatch();
  const accessToken = useSelector(store => store.userReducer.accessToken);
  const feeds = useSelector(store => store.feedReducers.feeds);
  const [feedLength, setFeedLength] = useState(
    _.isArray(feeds) ? feeds.length : 0,
  );
  const {selectedCategory} = useSelector(store => store.categoryReducers);
  const [currentFeedIndex, setCurrentFeedIndex] = useState(0);

  const _showReadMoreModal = item => {
    if (_.isArray(item.images) && item.images.length > 0) {
      let extraImages = item.images.filter(o => {
        return !o.is_cover;
      });
      let extraImagesArray = extraImages.map(o => o.file);
      if (extraImagesArray.length > 0) {
        item['extraImagesArray'] = extraImagesArray;
      }
    }

    setReadMoreData(item);
    setIsVisible(true);
    setIsHeader(false);
    setIsAnimation(false);
  };
  const _hideReadMoreModal = () => setIsVisible(false);
  const _showAutherDetailsModal = author => {
    setAutherData(author);
    setIsAutherShow(true);
    setIsHeader(false);
    setIsAnimation(false);
  };
  const _hideAutherDetailsModal = () => setIsAutherShow(false);

  const fadeAnim = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    if (currentFeedIndex >= feedLength - 2) {
      let skip = feedLength;
      let limit = 10;
      let id = selectedCategory.id;
      api.feed.getFeeds(accessToken, id, limit, skip).then(res => {
        if (res.status === 200) {
          res.json().then(async feeds => {
            dispatch(appendFeeds(feeds));
          });
        } else {
          return false;
        }
      });
    }
  }, [currentFeedIndex]);
  useEffect(() => {
    setFeedLength(_.isArray(feeds) ? feeds.length : 0);
  }, [feeds]);
  useEffect(() => {
    if (isHeader) {
      Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 500,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(fadeAnim, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false,
      }).start(() => {
        setIsAnimation(false);
      });
    }
  }, [isHeader]);

  const backAction = () => {
    if (isVisible) {
      setIsVisible(false);
    } else {
      navigation.goBack();
    }
    return true;
  };
  useEffect(() => {
    let backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  });
  const renderDescription = description => {
    var workArray = _.words(description);
    if (workArray.length > 60) {
      let chunkArray = _.chunk(workArray, 60)[0];
      let splitString = _.join(chunkArray, ' ');
      return splitString;
    } else {
      return description;
    }

    {
    }
  };
  const renderCoverImage = images => {
    if (!_.isArray(images) && images.length === 0) return false;
    let coverImage = images.filter(o => {
      return o.is_cover;
    });
    if (coverImage.length > 0) return coverImage[0].file;
    else return false;
  };
  const renderArticle = () => {
    const article = feeds.map((item, index) => {
      return (
        <TouchableOpacity
          key={item.id}
          style={{
            flex: 1,
            height: SCREEN_HEIGHT,
            width: SCREEN_WIDTH,
            backgroundColor: 'white',
          }}
          onPress={() => {
            setIsHeader(!isHeader);
            setIsAnimation(true);
          }}
          activeOpacity={1}>
          <View style={{flex: 0.5, backgroundColor: 'black'}}>
            <View
              style={{
                flex: 1,
                height: null,
                width: null,
                backgroundColor: '#424242',
              }}>
              <View
                style={{
                  flex: 1,
                  paddingVertical: '0%',
                }}>
                <Image
                  source={
                    !renderCoverImage(item.images)
                      ? ImageUrl.noImage
                      : {
                          uri: renderCoverImage(item.images),
                        }
                  }
                  style={{
                    flex: 1,
                    height: null,
                    width: null,
                    resizeMode: 'cover',
                  }}
                />
              </View>
            </View>
          </View>
          <View style={{flex: 0.5}}>
            <View
              style={{
                flex: 0.88,
                paddingHorizontal: 10,
              }}>
              <Text
                style={{
                  fontSize: Fonts.subHeading,
                  marginVertical: 10,
                  fontWeight: 'bold',
                  // fontFamily: 'roboto-regular',
                }}>
                {item.title}
              </Text>
              <Text
                style={{
                  textAlign: 'justify',
                  lineHeight: 20,
                  // fontFamily: 'roboto-regular',
                }}>
                {renderDescription(item.description)}

                {_.words(item.description).length > 60 && (
                  <Text
                    style={{
                      color: Colors.primaryColor,
                      // fontFamily: 'roboto-regular',
                      fontSize: 17,
                    }}
                    onPress={() => _showReadMoreModal(item)}>
                    {' '}
                    Read More...
                  </Text>
                )}
              </Text>
            </View>
            {item.author ? (
              <TouchableOpacity
                style={{
                  flex: 0.18,
                  flexDirection: 'row',
                  backgroundColor: Colors.gray,
                  paddingHorizontal: 10,
                }}
                onPress={() => _showAutherDetailsModal(item.author)}>
                <View style={{flex: 0.8, justifyContent: 'center'}}>
                  <Text
                    style={{
                      color: Colors.secondryColor,
                      // fontFamily: 'roboto-regular',
                      fontStyle: 'italic',
                      fontWeight: 'bold',
                    }}>
                    {item.author.name}
                  </Text>
                  <Text
                    style={{
                      color: Colors.secondryColor,
                      // fontFamily: 'roboto-regular',
                      fontStyle: 'italic',
                    }}>
                    {item.created_at}
                  </Text>
                </View>
              </TouchableOpacity>
            ) : null}
          </View>
        </TouchableOpacity>
      );
    });

    return article;
  };

  return (
    <Fragment>
      {isAnimation ? (
        <>
          <Animated.View
            style={{
              flexDirection: 'row',
              backgroundColor: Colors.white,
              position: 'absolute',
              top: 0,
              left: 0,
              zIndex: 1,
              minHeight: SCREEN_HEIGHT * 0.08,
              borderBottomWidth: 1,
              borderBottomColor: Colors.primaryColor,
              opacity: fadeAnim,
            }}>
            <TouchableOpacity
              style={{
                flex: 0.2,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => navigation.goBack()}>
              <Ionicon
                name="ios-arrow-back"
                size={25}
                color={Colors.primaryColor}
              />
            </TouchableOpacity>
            <View
              style={{
                flex: 0.6,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={ImageUrl.brandLogo}
                style={{
                  height: '100%',
                  width: '100%',
                  resizeMode: 'cover',
                }}
              />
            </View>
            <TouchableOpacity
              style={{
                flex: 0.2,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <AntDesign name="reload1" size={25} color={Colors.primaryColor} />
            </TouchableOpacity>
          </Animated.View>
          {/* <Animated.View
            style={{
              flexDirection: 'row',
              backgroundColor: Colors.white,
              position: 'absolute',
              bottom: 0,
              left: 0,
              zIndex: 1,
              minHeight: SCREEN_HEIGHT * 0.07,
              width: '100%',
              justifyContent: 'space-around',
              borderTopWidth: 1,
              borderTopColor: Colors.primaryColor,
              alignItems: 'center',
              opacity: fadeAnim,
            }}>
            <TouchableOpacity>
              <EvilIcons name="heart" size={30} color={Colors.primaryColor} />
            </TouchableOpacity>
            <TouchableOpacity>
              <EvilIcons name="comment" size={30} color={Colors.primaryColor} />
            </TouchableOpacity>

            <TouchableOpacity>
              <SimpleLineIcons
                name="paper-plane"
                size={22}
                color={Colors.primaryColor}
              />
            </TouchableOpacity>
          </Animated.View> */}
        </>
      ) : null}

      <Swiper
        style={{}}
        horizontal={false}
        loop={false}
        showsPagination={false}
        onIndexChanged={index => setCurrentFeedIndex(index)}>
        {renderArticle()}
      </Swiper>
      <ReadMoreModel
        isVisible={isVisible}
        _hideModal={_hideReadMoreModal}
        content={readMoreData}
      />
      {autherData ? (
        <AutherDetailsModel
          isVisible={isAutherShow}
          _hideModal={_hideAutherDetailsModal}
          content={autherData}
        />
      ) : null}
    </Fragment>
  );
}
