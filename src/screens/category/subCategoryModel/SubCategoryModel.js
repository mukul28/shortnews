import React, { useContext, useState, Fragment } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  ImageBackground,
  StatusBar,
  ScrollView,
} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { Modal, List, Divider, Title, Paragraph } from 'react-native-paper';
import { Colors, Fonts } from '../../../theme';
import { ImageUrl } from '../../../utils';
export function SubCategoryModel({
  isVisible,
  _hideModal,
  subCategory,
  handleSubCategory,
}) {
  return (
    <Modal
      visible={isVisible}
      onDismiss={_hideModal}
      contentContainerStyle={{ flex: 1, justifyContent: 'flex-end' }}
    >
      <TouchableOpacity style={{ flex: 0.5 }} onPress={_hideModal} />
      <View
        style={{
          flex: 0.5,
          backgroundColor: '#fff',
          justifyContent: 'space-between',
          borderTopStartRadius: 20,
          borderTopEndRadius: 20,
          paddingHorizontal: 0,
        }}
      >
        <View
          style={{
            flex: 0.1,
            borderTopStartRadius: 20,
            borderTopEndRadius: 20,
            justifyContent: 'center',
            backgroundColor: Colors.primaryColor,
          }}
        >
          <Title style={{ textAlign: 'center' }}>TRANDING</Title>
        </View>
        <ScrollView
          style={{
            flex: 0.8,
            flexDirection: 'column',
            alignContent: 'center',
          }}
        >
          {subCategory &&
            subCategory.map((obj, index) => {
              return (
                <Fragment key={index}>
                  <TouchableOpacity
                    style={{
                      flex: 0.2,
                      justifyContent: 'center',
                    }}
                    // onPress={() => }
                  >
                    <List.Item
                      key={obj}
                      title={obj}
                      left={(props) => (
                        <Ionicon
                          name="ios-sunny"
                          color={Colors.primaryColor}
                          size={30}
                          style={{ alignSelf: 'center', paddingRight: 10 }}
                        />
                      )}
                      right={(props) => (
                        <Ionicon
                          name="ios-arrow-forward"
                          size={25}
                          color={Colors.primaryColor}
                          style={{ paddingRight: 15 }}
                        />
                      )}
                    />
                  </TouchableOpacity>
                  <Divider />
                </Fragment>
              );
            })}
        </ScrollView>
        <View
          style={{
            flex: 0.05,
            // borderTopWidth: 1,
            // borderTopColor: Colors.primaryColor,
            // alignItems: 'center',
          }}
        />
      </View>
    </Modal>
  );
}
