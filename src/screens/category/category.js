import React, {useState, useEffect, useCallback, Fragment} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {SubCategoryModel} from './subCategoryModel';
import LinearGradient from 'react-native-linear-gradient';
import {Avatar} from 'react-native-paper';
import {useSelector, useDispatch} from 'react-redux';
import {ImageUrl} from '../../utils';
import {Colors, Fonts} from '../../theme';
import api from '../../api';
import {setCategory, setFeeds, setSelectedCategory} from '../../redux/actions';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import _ from 'lodash';
export function Category({navigation}) {
  const SCREEN_HEIGHT = Dimensions.get('window').height;
  const SCREEN_WIDTH = Dimensions.get('window').width;
  const {height} = Dimensions.get('window');
  const [searchText, setSearchText] = useState('');
  const [scrollHeight, setScrollHeight] = useState(height);
  const [isVisible, setIsVisible] = useState(false);
  const [subCategory, setSubCategory] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const dispatch = useDispatch();
  const {user, accessToken} = useSelector(store => store.userReducer);
  const {category} = useSelector(store => store.categoryReducers);
  const [categoryData, setCategoryData] = useState([]);
  const _hideSubCategoryModal = () => setIsVisible(false);
  const _showSubCategoryModal = () => {
    setIsVisible(true);
  };
  useEffect(() => {
    findstartswith(category, 'name', searchText);
  }, [searchText]);
  const findstartswith = (inputlist, searchkey, inputstring) => {
    let data = [];
    for (var il = 0; il < inputlist.length; il++) {
      if (
        _.lowerCase(inputlist[il][searchkey]).startsWith(
          _.lowerCase(inputstring),
        )
      ) {
        data.push(inputlist[il]);
      }
    }
    setCategoryData(data);
  };
  useEffect(() => {
    try {
      api.category.getCategory(accessToken).then(res => {
        console.log('Category -> res', res);
        if (res.status === 200) {
          res.json().then(async response => {
            setIsLoading(false);
            dispatch(setCategory(response));
            setCategoryData(response);
          });
        }
        if (res.status === 400) {
          res.json().then(response => {
            alert('Something Went wrong please try again after some time.');
            setIsLoading(false);
          });
        }
      });
    } catch (e) {
      alert('Something Went wrong please try again after some time.');
      setIsLoading(false);
    }
    if (Platform.OS === 'android') {
      if (SCREEN_HEIGHT > 800) {
        setScrollHeight(SCREEN_HEIGHT + 45);
        return;
      }

      setScrollHeight(SCREEN_HEIGHT - 2);
    } else {
      setScrollHeight(SCREEN_HEIGHT - 70);
    }
  }, []);

  const getVerticalOffSet = () => {
    if (Platform.OS === 'android') {
      if (SCREEN_HEIGHT > 800) {
        return -300;
      }
      return -220;
    } else {
      return 40;
    }
  };
  const handleSubCategory = useCallback(item => {
    if (item.subcategories && item.subcategories.length > 0) {
      _showSubCategoryModal(true);
      setSubCategory(item.subcategories);
    } else {
      api.feed.getFeeds(accessToken, item.id, 5, 0).then(res => {
        if (res.status === 200) {
          res.json().then(async feeds => {
            await dispatch(setFeeds(feeds));
            await dispatch(setSelectedCategory(item));
            navigation.navigate('Feed');
          });
        } else {
          return false;
        }
      });
    }
  });
  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={getVerticalOffSet()}
      style={{flex: 1}}
      enabled
      keyboardShouldPersistTaps={true}
      behavior="padding">
      <ScrollView
        contentContainerStyle={{
          height: scrollHeight,
        }}
        bounces={false}>
        <View
          style={{
            flex: 1,
            height: SCREEN_HEIGHT,
            width: SCREEN_WIDTH,
            backgroundColor: 'white',
          }}>
          <View
            style={{
              flex: 0.08,
              flexDirection: 'row',
              borderBottomWidth: 1,
              borderBottomColor: Colors.primaryColor,
            }}>
            <TouchableOpacity
              style={{
                flex: 0.2,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                navigation.navigate('Profile');
              }}>
              <Avatar.Image
                source={
                  user.profile_pic
                    ? {uri: user.profile_pic}
                    : ImageUrl.profileLogo
                }
                size={40}
              />
            </TouchableOpacity>
            <View
              style={{
                flex: 0.6,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={ImageUrl.brandLogo}
                style={{
                  height: '100%',
                  width: '100%',
                  resizeMode: 'cover',
                }}
              />
            </View>
            <View
              style={{
                flex: 0.2,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <FontAwesome
                name="bell-o"
                size={25}
                color={Colors.primaryColor}
              />
            </View>
          </View>
          {isLoading ? (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator color="#D7962E" size="large" />
            </View>
          ) : (
            <View style={{flex: 0.92}}>
              <View
                style={{
                  flex: 0.08,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderBottomWidth: Platform.OS === 'android' ? 0.2 : 0.3,
                  borderBottomColor: Colors.secondryColor,
                  marginBottom: 10,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '95%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Colors.gray,
                    borderRadius: 10,
                  }}>
                  <MaterialIcons name="search" color={'#BCBCBE'} size={20} />
                  <TextInput
                    style={{
                      paddingHorizontal: 5,
                      height: 40,
                      backgroundColor: Colors.gray,
                      borderRadius: 10,
                      width: '90%',
                    }}
                    onChangeText={text => setSearchText(text)}
                    value={searchText}
                    placeholder={'Search'}
                  />
                </View>
              </View>
              <View
                style={{
                  flex: 0.92,
                }}>
                <ScrollView
                  nestedScrollEnabled={true}
                  contentContainerStyle={{
                    flexWrap: 'wrap',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    paddingHorizontal: 18,
                  }}>
                  {categoryData &&
                    categoryData.map((item, index) => (
                      <TouchableOpacity
                        key={index}
                        style={{
                          height: 140,
                          width: '30%',
                          borderRadius: 7,
                          borderWidth: 1,
                          borderColor: Colors.primaryColor,
                          marginBottom: 10,
                        }}
                        onPress={() => handleSubCategory(item)}>
                        <View
                          style={{
                            flex: 1,
                          }}>
                          <ImageBackground
                            source={{uri: item.image}}
                            style={{
                              height: '100%',
                              width: '100%',
                              resizeMode: 'contain',
                              justifyContent: 'flex-end',
                            }}>
                            <LinearGradient
                              colors={[
                                '#FFF0',
                                '#FFF0',
                                '#FFF0',
                                '#FFF0',
                                '#FFF0',
                                '#FFF0',
                                '#FFF',
                                '#FFF',
                              ]}
                              style={{
                                flex: 1,
                                justifyContent: 'flex-end',
                                borderRadius: 7,
                              }}>
                              <Text
                                style={{
                                  fontSize: Fonts.smallText,
                                  marginHorizontal: 10,
                                  marginBottom: 2,
                                  fontWeight: 'bold',
                                  textAlign: 'center',
                                  color: Colors.secondryColor,
                                  // fontFamily: 'roboto-regular',
                                }}>
                                {item.name}
                              </Text>
                            </LinearGradient>
                          </ImageBackground>
                        </View>
                      </TouchableOpacity>
                    ))}
                </ScrollView>
              </View>
            </View>
          )}
        </View>
        {/* </View> */}
      </ScrollView>
      <SubCategoryModel
        isVisible={isVisible}
        _hideModal={_hideSubCategoryModal}
        subCategory={subCategory}
        handleSubCategory={handleSubCategory}
      />
    </KeyboardAvoidingView>
  );
}
