import axios from 'axios';
export default (apiUrl) => ({
  getCategory(token) {
    return fetch(`${apiUrl}/v1/category/categories/`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    });
  },
});
