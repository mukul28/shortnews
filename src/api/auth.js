import axios from 'axios';
export default (apiUrl) => ({
  registration(payload) {
    return fetch(`${apiUrl}/auth/users/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    });
  },
  login(payload) {
    return fetch(`${apiUrl}/auth/token/login/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    });
  },
  socialLogin(payload) {
    return fetch(`${apiUrl}/auth/social/auth/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    });
  },
  logout(token) {
    return fetch(`${apiUrl}/auth/token/logout/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    });
  },
  forgetPassword(payload) {
    return fetch(`${apiUrl}/auth/users/reset_password/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ payload }),
    });
  },
  resetPassword(payload) {
    return fetch(`${apiUrl}/auth/users/reset_password_confirm/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ payload }),
    });
  },
  resendOtp(payload) {
    return fetch(`${apiUrl}/auth/users/resend_activation/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ payload }),
    });
  },
  otpVarification(payload, email) {
    return fetch(`${apiUrl}/auth/verify-otp/${email}/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    });
  },
  updateProfile(token, payload) {
    console.log(
      'updateProfile -> token, payload',
      token,
      payload,
      `${apiUrl}/auth/users/me/`
    );

    return fetch(`${apiUrl}/auth/users/me/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
      body: JSON.stringify(payload),
    });
  },
});
