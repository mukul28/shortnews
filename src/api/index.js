import auth from './auth';
import category from './category';
import feed from './feed';
export const API_URL = 'http://18.221.193.90';
export default {
  auth: auth(API_URL),
  category: category(API_URL),
  feed: feed(API_URL),
};
