export default apiUrl => ({
  getFeeds(token, id, limit, skip) {
    return fetch(
      `${apiUrl}/v1/story/stories/?category=${id}&limit=${limit}&offset=${skip}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${token}`,
        },
      },
    );
  },
});
