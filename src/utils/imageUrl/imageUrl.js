export const ImageUrl = {
  brandLogo: require('../../assets/images/Observe-now-logo-1024_trans.png'),
  Google: require('../../assets/images/google-login.png'),
  noImage: require('../../assets/images/noImage.png'),
  One: require('../../assets/images/1.jpg'),
  Two: require('../../assets/images/2.jpeg'),
  Three: require('../../assets/images/3.jpg'),
  Four: require('../../assets/images/4.jpg'),
  Five: require('../../assets/images/5.png'),
  profileLogo: require('../../assets/images/profilelogo.png'),
};
