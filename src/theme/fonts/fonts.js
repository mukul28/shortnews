export const Fonts = {
  header: 30,
  heading: 25,
  subHeading: 17,
  textLarge: 18,
  Text: 16,
  smallText: 14,
};
