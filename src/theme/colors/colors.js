export const Colors = {
  primaryColor: '#FCB042',
  secondryColor: '#59585D',
  error: 'red',
  iconBlue: '#4A90E2',
  black: '#000000',
  white: '#FFFFFF',
  gray: '#E6E6E6',
  darkGray: '#696969',
};
